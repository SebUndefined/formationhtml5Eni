(function(window){
    "use strict;"
    
    if(!document.body.remove) {
        Element.prototype.remove = function() {
            this.parentNode.removeChild(this);
        };
    }
    



    console.log("Javascript okay");
    var titre = document.querySelector("#entete h1");

    titre.addEventListener("click", function(event){
        titre.textContent = "HTML, CSS & JavaScript";
        titre.style.color = "blue";
        titre.title = "Infobulle du titre";
    }, false);

    // validation email
    //attribution des box emails dans var
    var emailBox = document.querySelectorAll("[type='email'");
    //attribution du span dans var
    // var erreurEmailBox = document.querySelector(".erreur");
    //attribution des évemenents
    for(var i = 0; i < emailBox.length; i++) {
        emailBox[i].addEventListener("input", checkMyMail , false);
    }
    //fonction check des mails
    function checkMyMail(event){
            //erreurEmailBox.hidden = (emailBox[0].value === emailBox[1].value);
            if (emailBox[0].value === emailBox[1].value) {
                emailBox[0].setCustomValidity("");
                emailBox[1].setCustomValidity("");
                // erreurEmailBox.hidden = true;
            }
            else {
                emailBox[0].setCustomValidity("Adresses emails doivent être identiques");
                emailBox[1].setCustomValidity("Adresses emails doivent être identiques");
                // erreurEmailBox.hidden = false;
            }
    }
    checkMyMail();
    document.querySelector("#myform").checkValidity();

    //---------------------Tableau---------------------
    //get all table
    var table = document.querySelector("#table-pop tbody");
    //Apply event on tbody
    table.addEventListener("click", selectedClass, false);

    function selectedClass(event) {
        var ligne = event.target.parentNode; //ligne réélement cliqué
        if(!event.ctrlKey) {
            var selectedLines = event.currentTarget.querySelectorAll(".selected"); //reprend tous les éléments sélectionnés
            for(var i = 0; i < selectedLines.length; i++) 
            {
                if(selectedLines[i] != ligne || selectedLines.length > 1) {
                    selectedLines[i].classList.remove("selected"); //Supprime la classe
                }
            }
        }
        ligne.classList.toggle("selected"); //Ajoute la classe
    }

    //---------------------Suppression elements Tableau---------------------
    var deleteButton = document.querySelector("#deleteElem");
    deleteButton.addEventListener("click", deleteElement, false);

    function deleteElement(event) {
        var selectedLines = document.querySelectorAll(".selected");
        for (var i = 0; i < selectedLines.length; i++) {
            selectedLines[i].remove();
        }
    }

    //---------------------add elements Tableau---------------------
    var addButton = document.querySelector("#addElem");
    addButton.addEventListener("click", addElement, false);

    function addElement(event) {
        document.querySelector("#table-pop .saisie").hidden = false;
        document.querySelector("#validItem").hidden = false;
        document.querySelector("#deleteElem").hidden = true;
        document.querySelector("#addElem").hidden = true;
    }
    var validButton = document.querySelector("#validItem");
    validButton.addEventListener("click", addValidElement, false);
    function addValidElement(event) {
        var trSaisie = document.querySelector("#table-pop .saisie");
        var inputs = trSaisie.querySelectorAll("input");
        var tr = document.createElement("tr");
        for (var i = 0; i < inputs.length; i++) {
            if(inputs[i].value === "") {
                alert("saisie invalide");
                i = inputs.length + 1;
            }
            else
            {
                var td = tr.insertCell();
                td.textContent = inputs[i].value;
            }
        }
        if(i == inputs.length) {
            for(var i = 0; i < inputs.length; i++) {
                inputs[i].value = "";
            }
            document.querySelector("#table-pop tbody").appendChild(tr);
            document.querySelector("#table-pop .saisie").hidden = true;
            document.querySelector("#validItem").hidden = true;
            document.querySelector("#deleteElem").hidden = false;
            document.querySelector("#addElem").hidden = false;
        }
        
    }

})(window);